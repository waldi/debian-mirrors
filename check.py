# Copyright 2017 Bastian Blank <waldi@debian.org>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import aiohttp
import asyncio
import concurrent.futures
import datetime
import struct
import urllib


RSYNC_PORT = 873
RSYNCS_PORT = 1873

RSYNC_VERSION = 31


class RsyncClient:
    def __init__(self, connector, *, loop=None):
        self._connector = connector
        self._loop = loop

    async def get(self, url):
        req = RsyncClientRequest(url, loop=self._loop)
        reader, writer = await self.connect(req)
        resp = req.send(reader, writer)
        try:
            await resp.start()
        except:
            resp.close()
            raise
        return resp

    async def connect(self, req):
        if req.ssl:
            sslcontext = self._connector.ssl_context
        else:
            sslcontext = None

        hosts = await self._connector._resolve_host(req.host, req.port)
        exc = None

        for hinfo in hosts:
            try:
                host = hinfo['host']
                port = hinfo['port']
                return await asyncio.open_connection(
                    host, port,
                    ssl=sslcontext, family=hinfo['family'],
                    proto=hinfo['proto'], flags=hinfo['flags'],
                    server_hostname=hinfo['hostname'] if sslcontext else None,
                    loop=self._loop)
            except OSError as e:
                exc = e
        else:
            raise aiohttp.ClientOSError(exc.errno,
                                'Can not connect to %s:%s [%s]' %
                                (req.host, req.port, exc.strerror)) from exc


class RsyncClientRequest:
    def __init__(self, url, *, loop=None):
        if loop is None:
            loop = asyncio.get_event_loop()

        self.url = url
        self.loop = loop

        self.update(url)

    def update(self, url):
        """Update destination host, port and connection type (ssl)."""
        url_parsed = urllib.parse.urlsplit(url)

        # get host/port
        host = url_parsed.hostname
        if not host:
            raise ValueError('Host could not be detected.')

        try:
            port = url_parsed.port
        except ValueError:
            raise ValueError(
                'Port number could not be converted.') from None

        scheme = url_parsed.scheme
        self.ssl = scheme in ('rsyncs', )

        # set port number if it isn't already set
        if not port:
            if self.ssl:
                port = RSYNCS_PORT
            else:
                port = RSYNC_PORT

        path = url_parsed.path[1:]
        module = path.split('/', 1)[0]

        self.host, self.port, self.scheme = host, port, scheme
        self.path, self.module = path, module

    def send(self, reader, writer):
        return RsyncClientResponse(self, reader, writer)


class RsyncClientResponse:
    def __init__(self, req, reader, writer):
        self.req, self.reader, self.writer = req, reader, writer

    async def start(self):
        self.writer.write('@RSYNCD: {}.0\n'.format(RSYNC_VERSION).encode('ascii'))
        self.writer.write(self.req.module.encode('ascii'))
        self.writer.write(b'\n')
        await self.writer.drain()

        version_str = (await self.reader.readline()).decode('ascii')
        if not version_str.startswith('@RSYNCD: '):
            raise RuntimeError
        protocol_version = min(RSYNC_VERSION, int(version_str[9:11]))
        if protocol_version < 30:
            raise RuntimeError("Only rsync version 30 and 31 supported")

        while True:
            l = (await self.reader.readline()).rstrip()
            if l.startswith(b'@'):
                if l == b'@RSYNCD: OK':
                    break
                raise RuntimeError(l)

        self.writer.write(b'--server\x00--sender\x00-Le.Lsfx\x00.\x00')
        self.writer.write(self.req.path.encode('ascii'))
        self.writer.write(b'\x00\x00')
        await self.writer.drain()

        await self.reader.readexactly(1)
        await self.reader.readexactly(4)

        if protocol_version >= 31:
            # Read timeout
            await self.multiplex_read()

        await self.multiplex_write(0, b"\x00\x00\x00\x00")

#        if protocol_version <= 30:
#            # Read unknown
#            await self.multiplex_read()

        # Read file entry
        message, data = await self.multiplex_read()
        if (message != 0):
            raise RuntimeError(data)

        await self.multiplex_write(0, b'\x01\x00\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')
        await self.multiplex_write(0, b'\x00')

    async def text(self):
        message, data = await self.multiplex_read()
        if (message):
            raise RuntimeError(data)
        size = struct.unpack_from("<I", data, 19)
        token = data[23:23+size[0]]

        await self.multiplex_write(0, b'\x00\x00\x00')

        await self.multiplex_read()
        await self.multiplex_read()

        await self.multiplex_write(0, b'\x00')

        return token.decode('ascii')

    def close(self):
        self.writer.close()

    async def multiplex_read(self):
        m = await self.reader.readexactly(4)
        s = struct.unpack("<HBB", m)
        size = (s[1] << 16) + s[0]
        message = s[2] - 7
        data = await self.reader.readexactly(size)
        return message, data

    async def multiplex_write(self, message, data):
        size = len(data)
        s = struct.pack("<HBB", size & 0xffff, size >> 16, message + 7)
        self.writer.write(s)
        self.writer.write(data)
        await self.writer.drain()



async def http(session, url):
    try:
        with aiohttp.Timeout(20):
            resp = await session.get(url)
            ret = await resp.text()
            return url, ret

    except Exception as e:
        return url, e


async def rsync(session, url):
    try:
        with aiohttp.Timeout(20):
            client = RsyncClient(session.connector)
            resp = await client.get(url)
            ret = await resp.text()
            resp.close()
            return url, ret

    except Exception as e:
        return url, e


def _read_rfc822(f, cls):
    entries = []
    eof = False

    while not eof:
        e = cls()
        last = None
        lines = []
        while True:
            line = f.readline()
            if not line:
                eof = True
                break
            # Strip comments rather than trying to preserve them
            if line[0] == '#':
                continue
            line = line.strip('\n')
            if not line:
                break
            if line[0] in ' \t':
                if not last:
                    raise ValueError('Continuation line seen before first header')
                lines.append(line.lstrip())
                continue
            if last:
                e[last] = '\n'.join(lines)
            i = line.find(':')
            if i < 0:
                raise ValueError(u"Not a header, not a continuation: ``%s''" % line)
            last = line[:i]
            lines = [line[i + 1:].lstrip()]
        if last:
            e[last] = '\n'.join(lines)
        if e:
            entries.append(e)

    return entries


def _parse_date(d):
    return datetime.datetime.strptime(d, '%a %b %d %H:%M:%S UTC %Y')


with open('Mirrors.masterlist') as f:
    mirrorlist = _read_rfc822(f, dict)


async def test():
    with aiohttp.ClientSession() as session:
        resp = []

        resp.append(http(session, 'http://ftp-master.debian.org/debian/project/trace/master'))

        for mirror in mirrorlist:
            name = mirror['Site']
            t = mirror.get('Type', '').lower()

            if t not in ('push-primary', 'push-secondary', 'leaf'):
                print("ignore: {}".format(name))
                continue

            if 'Archive-http' in mirror:
                resp.append(http(session, 'http://{}/{}/project/trace/master'.format(name, mirror['Archive-http'])))
            if 'Archive-rsync' in mirror:
                resp.append(rsync(session, 'rsync://{}/{}/project/trace/master'.format(name, mirror['Archive-rsync'])))

        pending = set()
        done_url = {}
        done_error = {}

        while resp or pending:
            if len(pending) < 200:
                l = 200 - len(pending)
                pending.update(resp[:l])
                resp = resp[l:]
            done, pending = await asyncio.wait(pending, return_when=concurrent.futures.FIRST_COMPLETED)
            for d in done:
                result = d.result()
                if issubclass(type(result[1]), Exception):
                    done_error[result[0]] = result[1]
                else:
                    done_url[result[0]] = result[1]

        date_reference = _parse_date(done_url['http://ftp-master.debian.org/debian/project/trace/master'].split("\n")[0])
        print("Master: {}".format(date_reference))

        print("Out out date:")

        for url, text in sorted(done_url.items()):
            try:
                date = _parse_date(text.split("\n")[0])
                delta = date_reference - date
                if delta.days:
                    print("{}:".format(url))
                    print("    {}".format(text.split("\n")[0]))
            except Exception as e:
                done_error[url] = e

        print("Failed:")

        for url, e in sorted(done_error.items()):
            print("{}:".format(url))
            print("    {!r}".format(e))


async def test1():
    await asyncio.wait_for(test(), timeout=300)


loop = asyncio.get_event_loop()
loop.run_until_complete(test1())
loop.close()
